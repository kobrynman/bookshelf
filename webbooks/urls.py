from django.conf import settings
from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout
from django.core.urlresolvers import reverse_lazy
from django.views.decorators.csrf import csrf_exempt

from webbooks import views

add_remove_book_ajax = login_required(csrf_exempt(views.AddRemoveBookAjaxView.as_view()), login_url=reverse_lazy('login'))
set_rating_ajax = login_required(csrf_exempt(views.SetRatingAjaxView.as_view()), login_url=reverse_lazy('login'))
delete_favorite_book_ajax = login_required(csrf_exempt(views.DeleteFavoriteDookAjax.as_view()), login_url=reverse_lazy('login'))

urlpatterns = patterns('',
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^registration/$', views.registration,  name='registration'),
    url(r'^$', views.BooksListView.as_view(), name='books_list'),
    url(r'^books/(?P<pk>\d+)/$', views.BookDetailView.as_view(), name='book_detail'),
    url(r'^profile/(?P<pk>\d+)/$', login_required(views.BooksProfileView.as_view()), name='book_profile'),

    #=== Ajax
    url(r'^add_remove_book_ajax/$', add_remove_book_ajax),
    url(r'^set_rating_ajax/$', set_rating_ajax),
    url(r'^delete_favorite_book_ajax/$', delete_favorite_book_ajax),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
