from django.db import models
from django.contrib.auth.models import User

class Genre(models.Model):
    genre = models.CharField(max_length=60, unique=True)

    def __unicode__(self):
        return self.genre


class Books(models.Model):
    tittle = models.CharField(max_length=120)
    author = models.CharField(max_length=60)
    cover = models.ImageField(upload_to='books_images/')
    genre = models.ForeignKey(Genre)
    annotation = models.CharField(max_length=200)
    books_rating = models.ManyToManyField(User, through='BooksRating', related_name='books_rating')
    favourite_books = models.ManyToManyField(User, through='FavouriteBooks', related_name='favourite_books')

    def __unicode__(self):
        return self.tittle

class BooksRating(models.Model):
    books = models.ForeignKey(Books)
    user = models.ForeignKey(User)
    rating = models.DecimalField(max_digits=2, decimal_places=1)

    class Meta:
        unique_together = ('books', 'user',)

    def __unicode__(self):
        return u'%s %s' % (self.books, self.rating,)

class FavouriteBooks(models.Model):
    books = models.ForeignKey(Books)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.books
