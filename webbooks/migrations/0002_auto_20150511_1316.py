# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webbooks', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='booksrating',
            unique_together=set([('books', 'user')]),
        ),
    ]