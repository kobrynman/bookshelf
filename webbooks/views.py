import json
from django.views.generic import ListView, CreateView, DetailView, View
from django.shortcuts import redirect, render
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.db.models import Avg
from webbooks.models import Genre, Books, FavouriteBooks, BooksRating
from webbooks.forms import CreateNewUser



def registration(request):
    if request.method == 'POST':
        form = CreateNewUser(request.POST)
        if form.is_valid():
            form.save()
            user = auth.authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
            auth.login(request, user)
            return HttpResponseRedirect('/')
    else:
        form = CreateNewUser()
    return render(request, 'registration/registration.html', {
        'form': form,
    })

class BooksListView(ListView):
    template_name = 'books_list.html'
    paginate_by = 6
    model = Books

    def get_queryset(self):
        genres_id = self.request.GET.get('genre')
        books = Books.objects.all()
        if genres_id:
            books = books.filter(genre__id=genres_id)
        return books


    def get_context_data(self, *args, **kwargs):
        context = super(BooksListView, self).get_context_data(*args, **kwargs)
        context['genres'] = Genre.objects.all()
        context['top'] = Books.objects.all()
        return context


class BookDetailView(DetailView):
    template_name = 'webbooks/books_details.html'
    context_object_name = 'book'
    model = Books

    def get_context_data(self, *args, **kwargs):
        context = super(BookDetailView, self).get_context_data(*args, **kwargs)
        context['genres'] = Genre.objects.all()
        context['top'] = Books.objects.all()
        return context


class BooksProfileView(ListView):
    model = Books
    template_name = 'webbooks/books_profile.html'
    paginate_by = 3

    def get_queryset(self):
        genres_id = self.request.GET.get('genre')
        books = User.objects.get(id=self.kwargs['pk']).favourite_books.all()
        if genres_id:
            books = books.filter(genre__id=genres_id)
        return books

    def get_context_data(self, *args, **kwargs):
        context = super(BooksProfileView, self).get_context_data(*args, **kwargs)
        context['genres'] = Genre.objects.all()
        context['top'] = Books.objects.all()
        return context

#=== ajax
class AddRemoveBookAjaxView(View):
    def post(self, *args, **kwargs):
        id = self.request.POST.get('id')
        polygons = self.request.POST.get('action')
        try:
            book = Books.objects.get(id=id)
            if polygons == "add":
                FavouriteBooks.objects.create(books=book, user=self.request.user)
            if polygons == "remove":
                FavouriteBooks.objects.filter(books=book, user=self.request.user).delete()
        except Books.DoesNotExist, FavouriteBooks.DoesNotExist:
            pass
        return HttpResponse('')

class SetRatingAjaxView(View):
    def post(self, *args, **kwargs):
        id = self.request.POST.get('id')
        rating_val = self.request.POST.get('rating_val')

        book = get_object_or_404(Books, pk=id)
        BooksRating.objects.update_or_create(books=book, user=self.request.user, defaults={'rating': rating_val})

        data = {
            'view': book.books_rating.count(),
            'mark': str(BooksRating.objects.get(books=book, user=self.request.user).rating),
            'total_mark': str(BooksRating.objects.filter(books=book).aggregate(Avg('rating'))['rating__avg'])
        }
        return HttpResponse(json.dumps(data), content_type="application/json")


class DeleteFavoriteDookAjax(View):
    def post(self, *args, **kwargs):
        book_id = self.request.POST.get('id')
        book = get_object_or_404(Books, pk=book_id)
        favourite_books = get_object_or_404(FavouriteBooks, books=book.id, user=self.request.user.id)
        favourite_books.delete()
        return HttpResponse("")
