from django.contrib import admin
from webbooks.models import Books, Genre, FavouriteBooks, BooksRating


# class FavouriteBooksInline(admin.TabularInline):
#     model = FavouriteBooks
#     extra = 1
#
# class BooksAdmin(admin.ModelAdmin):
#     inlines = [FavouriteBooksInline]


admin.site.register(Books)

admin.site.register(Genre)

admin.site.register(BooksRating)
