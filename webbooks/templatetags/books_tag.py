from django import template
from webbooks.models import BooksRating
from django.db.models import Sum, Avg, Count, Max, Min


register = template.Library()

@register.filter
def rating_books(book):
    return BooksRating.objects.filter(books=book).aggregate(Avg('rating'))['rating__avg']

@register.filter
def you_rating_books(book, user):
    if BooksRating.objects.filter(books=book, user=user).exists():
        return BooksRating.objects.get(books=book, user=user).rating
    else:
        return 'None'
